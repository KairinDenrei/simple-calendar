// Import the discord.js module
const Discord = require('discord.js');
const prefix = '!';

// Create an instance of Discord that we will use to control the bot
const bot = new Discord.Client();

// Token for your bot, located in the Discord application console - https://discordapp.com/developers/applications/me/
const token = 'NDM5NDM2MzkyOTU1NjQxODY3.DcijLA.GAQEKnxnj33HYhDPuKWTjrLnAxM';


var channel = 'timetable';

// Gets called when our bot is successfully logged in and connected
bot.on('ready', () => {
    //console.log('Hello World!');
});

// Event to listen to messages sent to the server where the bot is located
bot.on('message', message => {
    // So the bot doesn't reply to iteself
    if (!message.content.startsWith(prefix) || message.author.bot) {
        return;
    } else {
        // Get the user's message excluding the `!`
        var cmd = message.content.substring(1).split(' ');

        switch (cmd[0]) {
            case 'ping':
                message.reply('pong');
            break;

            case 'event':
                if(channel != '') {
                    var event = message.content.substring(7),
                        user = message.author;

                    bot.channels
                        .find("name", channel)
                        .send(event + '\n\nOrganizer: ' + user + '\n\nSigned Up:\n\nBackup:\n\nMaybe:\n\nNo:')
                        .then(function (message) {
                            message.react("➕");
                            message.react("❔");
                            message.react("➖");
                    });
                } else {
                    message.reply('Use **!bind** to set a channel as timetable.');
                }
            break;

            case 'bind':
                channel = cmd[1];
                message.reply('Set #' + cmd[1] + ' as channel for events.');
            break;
        }
    }
});

function decodeMessage(msg) {
    var obj = msg.split('\n\n'),
        userObj = {
            'Event': obj[0],
            'Commander': obj[1],
            'Members': {}
        };

    for(var i = 2; i < obj.length; i++) {
        var arr = obj[i].split('\n'),
            key = arr[0].trim();

        arr.shift();

        for(var j = 0; j < arr.length; j++) {
            arr[j] = arr[j].split('.')[1].trim();
        }

        userObj['Members'][key] = arr;
    }

    return userObj;
}

function updateMessage(obj) {
    console.log(obj);

    var str = obj['Event'] + '\n\n ' + obj['Commander'];

    for(var k in obj['Members']) {
        if (obj['Members'].hasOwnProperty(k)) {

            if(obj['Members'][k].length > 0) {
                for(var i=0; i < obj['Members'][k].length; i++) {
                    var index = parseInt(i + 1);

                    str += '\n\n' + k + '\n' + index + '. ' + obj['Members'][k][i];
                }

            } else {
                str += '\n\n' + k;
            }
        }
    }

    return str;
}


bot.on('messageReactionAdd', (reaction, user) => {
    if(user != '<@439436392955641867>') {
        var msgObj = decodeMessage(reaction.message.toString());

        switch (reaction.emoji.name) {
            case "➕":
                if(msgObj['Members']['Signed Up:'].length < 11) {
                    msgObj['Members']['Signed Up:'].push(user);
                } else {
                    msgObj['Members']['Backup:'].push(user);
                }
            break;

            case "➖":
                msgObj['Members']['No:'].push(user);
            break;

            case "❔":
                msgObj['Members']['Maybe:'].push(user);
            break;
        }

        reaction.message.edit(updateMessage(msgObj));
        reaction.remove(user);
    }
});


bot.login(token);